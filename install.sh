#!/bin/bash

set -e

ROOT_SCRIPT_PATH="$(pwd)"

PIP_CMD="pip3"
PYTHON_VENV_DIR="venv"
PYTHON_VENV_NAME="tests"

#GAITSET_DATASET_PATH="CASIA-B"
GAITSET_DATASET_FILE="GaitDatabases.asp"

install_venv() {
	cd $ROOT_SCRIPT_PATH
	
	if ! command -v "conda" &> /dev/null; then
		echo "Please install anaconda or miniconda"
		echo "You can download miniconda at https://docs.conda.io/en/latest/miniconda.html#linux-installers"
		return 1
	fi

	if [ -d "$PYTHON_VENV_DIR" ]; then
		return 0
	fi

	mkdir $PYTHON_VENV_DIR
	cd $PYTHON_VENV_DIR

	conda create -p $PYTHON_VENV_NAME python=3.7.1 ipython

	CUR_STATE="INSTALL_VENV"
	save_state
}

activate_env() {
	cd $ROOT_SCRIPT_PATH

	if [ ! -d $PYTHON_VENV_DIR/$PYTHON_VENV_NAME ]; then
		install_venv
	fi

	chmod +x "$(conda info --base)"/etc/profile.d/conda.sh
	source "$(conda info --base)"/etc/profile.d/conda.sh

	#if command -v "deativate" &> /dev/null; then
	#	return 0
	#fi

	#cd $PYTHON_VENV_DIR
	conda activate ./$PYTHON_VENV_DIR/$PYTHON_VENV_NAME
	#source $PYTHON_VENV_DIR/$PYTHON_VENV_NAME/bin/activate
}

save_state() {
	cd $ROOT_SCRIPT_PATH
	echo $CUR_STATE > .install_state
}

install_gaitset () {
	echo "Cloning GaitSet..."
	
	cd $ROOT_SCRIPT_PATH
	if [ -d "./GaitSet" ]; then
		echo "GaitSet is already installed. Updating code..."
		cd GaitSet
		git pull
		cd ..
		return 0
	else
		git clone --recursive https://github.com/AbnerHqC/GaitSet
	fi

	CUR_STATE="INSTALL_GAITSET"
	save_state
}

download_casia_b () {
	echo "Downloading CASIA-B dataset (623M)..."

	cd $ROOT_SCRIPT_PATH
	if [ ! -d "casia-b" ]; then
		mkdir casia-b
	fi

	cd casia-b

	wget -c http://www.cbsr.ia.ac.cn/GaitDatasetB-silh.zip
	unzip GaitDatasetB-silh.zip

	CUR_STATE="DOWNLOAD_DATASET"
	save_state
}

install_gaitset_libs () {
	echo "Installing GaitSet..."
	#CUR_STATE=""
	
	cd $ROOT_SCRIPT_PATH
	cd GaitSet
	
	if ! command -v "nvcc" &> /dev/null ; then
		echo "NVIDIA CUDA is not installed. Please install NVIDIA CUDA"
		return 1
	fi
	$PIP_CMD install torch xarray

	if [ ! -f "$GAITSET_DATASET_FILE" ]; then
		wget "http://www.cbsr.ia.ac.cn/english/Gait%20Databases.asp" -o $GAITSET_DATASET_FILE
	fi
	CUR_STATE="INSTALL_GAITSET_LIBS"
	save_state
}

install_tf_gait() {
	echo "Cloning TensorFlow gait recognition..."

	cd $ROOT_SCRIPT_PATH
	if [ -d "./gait-recognition" ]; then
		echo "TF gait recognition is already installed. Updating code..."
		cd gait-recognition
		git pull
		cd ..
		return 0
	else
		git clone --recursive https://github.com/marian-margeta/gait-recognition gait_recognition
	fi
	
	if [ ! -f "./gait_recognition/__init__.py" ]; then
		touch gait_recognition/__init__.py
	fi
	
	CUR_STATE="INSTALL_TF_GAIT"
	save_state
}

install_tf_gait_libs() {
	echo "Installing TensorFlow gait recognition..."

	cd $ROOT_SCRIPT_PATH
	cd gait_recognition

	$PIP_CMD install tensorflow==1.13.1

	$PIP_CMD install numpy scipy==1.1.0 Pillow matplotlib

	echo "---------"
	echo "Please download the model manually from the cloud and place it in the gait-recognition directory"
	echo "https://drive.google.com/file/d/1lup13q5lTzsbrRZafpNbVF8uUyblMpZ3/view?usp=sharing"
	echo "---------"

	CUR_STATE="INSTALL_TF_GAIT_LIBS"
	save_state
}

if [ ! -f ".install_state" ]; then
	touch .install_state
fi

CUR_STATE="$(cat .install_state)"

activate_env

case $CUR_STATE in
	"INSTALL_VENV")
		activate_env

		install_tf_gait
		install_tf_gait_libs
		;;
	
	"INSTALL_TF_GAIT")
		activate_env

		install_tf_gait_libs
		;;
	
	"INSTALL_TF_GAIT_LIBS")
		echo "Nothing to do..."
		;;

	*)
		activate_env

		install_tf_gait
		install_tf_gait_libs
		;;
esac
