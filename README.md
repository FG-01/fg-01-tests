# \[FG-01 GAIT TEST\]

This repository will help you to deploy and run https://github.com/marian-margeta/gait-recognition DNN.

Note that this recognition software differs from actual deployed systems. Always check which gait recognition software is installed in your city. Do your own research.

## 1. Requirements

* Miniconda/Anaconda (https://conda.io/en/latest/miniconda.html)
* ffmpeg
* git

## 2. Setup

Run `./install.sh` and the gait recognition NN will be installed in the `gait_recognition` directory.

Open `gait_recognition` folder and download the pretrained model (https://drive.google.com/file/d/1lup13q5lTzsbrRZafpNbVF8uUyblMpZ3/view?usp=sharing) to the directory root.

If you're going to run pose estimation, open `gait_recognition/models` folder and download MPII+LSP model (https://drive.google.com/file/d/1bNoZkuI0TCqf_DV613SOAng3p6Y0Si6a/view?usp=sharing).

## 3. Recording

Find a suitable place to record your gait. Footage will be cut to square (299x299 px), so the camera shouldn't be too close or too far away. The person should be in the center of the frame. Make sure that the body is fully in frame.

Record your reference gait (in usual clothes). Other results will be compared with this one.

Repeat the same process, but with different clothes. Try to walk naturally to get the most accurate results.

## 4. Video preparation

Run `./decode_frames.sh <video>` to decode video frames. Output will be saved to `frames_<video>` folder.

If you wish to cut the video, specify start time and end time in the arguments (hh:mm:ss).

## 5. Running

Execute `./run.sh` script. You can specify as many videos (folders) as you want in the arguments. Note that the NN will attempt to process each video frames in bulk.

## 6. Results

Run `./show_results.sh <reference_folder> <video1> [<video2>]`. All vectors will be compared with the reference one.

Note that results may vary due to unpredicted factors, record multiple videos of gate to eliminate this factor.

## Tips to improve spoofing

* Wear baggy clothes
* Put hands in pockets
* Wear different backpacks/bags
* Increase FG-01 weight (pick different material)
* Try wearing different boots
