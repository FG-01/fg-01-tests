import sys

gait_dir = "gait_recognition"

sys.path.append(gait_dir)

import os
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from scipy.misc import imresize, imread

from gait_recognition.human_pose_nn import HumanPoseIRNetwork
from parse_frames import parse_folder_numpy

if len(sys.argv) < 2:
    print("Usage: pose.py video_folder")
    sys.exit(1)

net_pose = HumanPoseIRNetwork()
net_pose.restore(os.path.join(gait_dir, 'models/MPII+LSP.ckpt'))

print("-----------> Parsing video frames...")
video_frames = parse_folder_numpy(sys.argv[1], (299, 299))
print("-----------> Finished parsing frames!")

y, x, a = net_pose.estimate_joints(video_frames)

video_frames = (video_frames).astype(np.uint8)

colors = ['r', 'r', 'b', 'm', 'm', 'y', 'g', 'g', 'b', 'c', 'r', 'r', 'b', 'm', 'm', 'c']

save_dir = sys.argv[1] + "_poses"

for j in range(len(x)):
    for i in range(16):
        if i < 15 and i not in {5, 9}:
            plt.plot([x[j][i], x[j][i + 1]], [y[j][i], y[j][i + 1]], color = colors[i], linewidth = 5)

    plt.axis('off')
    plt.imshow(video_frames[j])

    if not os.path.isdir(save_dir):
        os.mkdir(save_dir)
    plt.savefig(os.path.join(save_dir, str(j) + ".jpg"))
    plt.clf()

print("Results have been written to", save_dir)
