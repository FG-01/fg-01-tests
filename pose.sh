#!/bin/bash

set -e

ROOT_SCRIPT_PATH="$(pwd)"

PYTHON_CMD="python3"
PIP_CMD="pip3"
PYTHON_VENV_DIR="venv"
PYTHON_VENV_NAME="tests"

activate_env() {
	cd $ROOT_SCRIPT_PATH

	if [ ! -d $PYTHON_VENV_DIR/$PYTHON_VENV_NAME ]; then
		install_venv
	fi

	chmod +x "$(conda info --base)"/etc/profile.d/conda.sh
	source "$(conda info --base)"/etc/profile.d/conda.sh

	#if command -v "deativate" &> /dev/null; then
	#	return 0
	#fi

	#cd $PYTHON_VENV_DIR
	conda activate ./$PYTHON_VENV_DIR/$PYTHON_VENV_NAME
	#source $PYTHON_VENV_DIR/$PYTHON_VENV_NAME/bin/activate
}

prepareVideo() {
	cd $ROOT_SCRIPT_PATH

	echo "Decoding video frames..."
	decode_frames $1
}

runPose() {
	cd $ROOT_SCRIPT_PATH

	echo "Starting video recognition..."
	$PYTHON_CMD pose.py ${1%%.*}
}

if [ "$#" -lt 1 ]; then
	echo "Usage: pose.sh frames_dir [frames_dir_1, ...]"
	exit 1
fi

#prepareVideo $1
activate_env
for var in "$@"; do
	runPose $var
done
