#!/bin/bash

if [ -z $1 ]; then
	echo "Usage: decode_frames.sh video [start_time] [end_time]"
	exit 1
fi

if [ ! -d "frames_${1%%.*}" ]; then
	mkdir frames_${1%%.*}
else
	exit 0
fi

if [ -z $2 ]; then
	ffmpeg -i $1 frames_${1%%.*}/%06d.jpg
else
	ffmpeg -i $1 -ss $2 -to $3 frames_${1%%.*}/%06d.jpg
fi

echo "Results have been written to the frames_${1%%.*} folder."
