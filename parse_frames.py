import tensorflow as tf
import os
import numpy
from glob import glob
from PIL import Image, UnidentifiedImageError

def parse_folder(folder):
    # DEPRECATED: convertion to numpy array takes too long
    pattern = os.path.join(folder, '*')
    files_list = sorted(glob(pattern), key=os.path.getctime)
    frames = tf.train.string_input_producer(files_list)

    reader = tf.WholeFileReader()
    key, value = reader.read(frames)
    print("[FINISHED] reading files")

    raw_img = tf.image.decode_jpeg(value, channels=3)
    print("[FINISHED] decoding frames")

    images = []
    #for f in frames:
        #with open(os.path.join(folder, f), 'r') as raw:
    res = tf.image.convert_image_dtype(raw_img, dtype=tf.float32)
    print("[FINISHED] converting frames. res size is")

    #video = tf.stack(images, axis=0)

    init = tf.global_variables_initializer()
    with tf.Session() as sess:
        sess.run(init)
        tf.train.start_queue_runners()

        fr = []
        for i in range(len(files_list)):
            fr.append(res.eval(session=sess))
        video = tf.stack(fr, axis=0)
        res = video.eval(session=sess)
        print("[FINISHED] generating numpy array")
    return res

def parse_folder_numpy(folder, size):
    pattern = os.path.join(folder, '*')
    files_list = sorted(glob(pattern))

    imgs = []
    for f in files_list:
        try:
            raw = Image.open(f).convert('RGB')
        except UnidentifiedImageError:
            continue
        
        new_w = int(float(size[1])/float(raw.size[1]) * raw.size[0])
        
        raw_res = raw.resize((new_w, size[1]), Image.ANTIALIAS)

        left, top, right, bottom = (raw_res.size[0] - size[0]) / 2, (raw_res.size[1] - size[1]) / 2, (raw_res.size[0] + size[0]) / 2, (raw_res.size[1] + size[1]) / 2
        raw_cr = raw_res.crop((left, top, right, bottom))

        if raw_cr.size != size:
            res = raw.resize(size, Image.ANTIALIAS)
        else:
            res = raw_cr

        img = numpy.array(res, dtype='float32')
        imgs.append(img)

        del raw
        del new_w
        del raw_res
        del raw_cr
        del res
        del img

    st = numpy.stack(imgs, axis=0)
    del imgs
    return st


