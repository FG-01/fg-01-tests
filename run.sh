#!/bin/bash

set -e

ROOT_SCRIPT_PATH="$(pwd)"
GAITSET_DATASET_PATH="CASIA-B"
GAITSET_DATASET_OUTPUT="output"
GAITSET_PREPARE_WORKERS=4
GAITSET_BATCH_SIZE=1

PYTHON_CMD="python3"
PIP_CMD="pip3"
PYTHON_VENV_DIR="venv"
PYTHON_VENV_NAME="tests"

activate_env() {
	cd $ROOT_SCRIPT_PATH

	if [ ! -d $PYTHON_VENV_DIR/$PYTHON_VENV_NAME ]; then
		install_venv
	fi

	chmod +x "$(conda info --base)"/etc/profile.d/conda.sh
	source "$(conda info --base)"/etc/profile.d/conda.sh

	#if command -v "deativate" &> /dev/null; then
	#	return 0
	#fi

	#cd $PYTHON_VENV_DIR
	conda activate ./$PYTHON_VENV_DIR/$PYTHON_VENV_NAME
	#source $PYTHON_VENV_DIR/$PYTHON_VENV_NAME/bin/activate
}

prepareGaitset() {
	if [ ! -d $1 ]; then
		mkdir $1
	else
		echo "Folder $1 already exists"
		return 1
	fi
	if [ ! -d $1/$2 ]; then
		mkdir $1/$2
	else
		echo "Folder $1/$2 already exists"
		return 1
	fi
	if [ ! -d $1/$2/$3 ]; then
		mkdir $1/$2/$3
	else
		echo "Folder $1/$2/$3 already exists"
		return 1
	fi

	$PYTHON_CMD pretreatment.py --input_path=$GAITSET_DATASET_PATH --output_path=$GAITSET_DATASET_OUTPUT --worker_num=$GAITSET_PREPARE_WORKERS
}

trainGaitset() {
	$PYTHON_CMD train.py
}

runGaitset() {
	$PYTHON_CMD test.py --batch_size=$GAITSET_BATCH_SIZE
}

prepareVideo() {
	cd $ROOT_SCRIPT_PATH

	echo "Decoding video frames..."
	decode_frames $1
}

runTFGait() {
	cd $ROOT_SCRIPT_PATH

	echo "Starting video recognition..."
	$PYTHON_CMD run_test.py ${1%%.*}
}

if [ "$#" -lt 1 ]; then
	echo "Usage: run.sh frames_dir [frames_dir_1, ...]"
	exit 1
fi

#prepareVideo $1
activate_env
for var in "$@"; do
	runTFGait $var
done
