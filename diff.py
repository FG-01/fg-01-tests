import numpy
import sys
import os

def calculate_probability(a, b):
    a = numpy.concatenate((a[0], a[1][0][0], a[1][1][0]))
    b = numpy.concatenate((b[0], b[1][0][0], b[1][1][0]))
    diff = numpy.absolute(b - a)
    diff *= -100
    diff += 100
    res = numpy.average(diff)
    return res

def print_result(percentage):
    GREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

    color = GREEN
    if percentage < 70:
        color = GREEN
    elif percentage < 80:
        color = WARNING
    else:
        color = FAIL

    print(color + str(percentage) + ENDC)

def load(directory):
    return numpy.load(os.path.join(directory, "vector.npy"), allow_pickle=True)

if len(sys.argv) < 2:
    print("Usage: diff.py folder1 folder2")
    exit(1)

per = calculate_probability(load(sys.argv[1]), load(sys.argv[2]))
print_result(per)
