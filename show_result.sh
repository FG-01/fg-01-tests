#!/bin/bash

set -e

ROOT_SCRIPT_PATH="$(pwd)"

PYTHON_CMD="python3"
PIP_CMD="pip3"
PYTHON_VENV_DIR="venv"
PYTHON_VENV_NAME="tests"

activate_env() {
	cd $ROOT_SCRIPT_PATH

	if [ ! -d $PYTHON_VENV_DIR/$PYTHON_VENV_NAME ]; then
		install_venv
	fi

	chmod +x "$(conda info --base)"/etc/profile.d/conda.sh
	source "$(conda info --base)"/etc/profile.d/conda.sh

	#if command -v "deativate" &> /dev/null; then
	#	return 0
	#fi

	#cd $PYTHON_VENV_DIR
	conda activate ./$PYTHON_VENV_DIR/$PYTHON_VENV_NAME
	#source $PYTHON_VENV_DIR/$PYTHON_VENV_NAME/bin/activate
}

print_result() {
	cd $ROOT_SCRIPT_PATH

	echo "$2: $($PYTHON_CMD diff.py $1 $2) match"
}

if [ "$#" -lt 2 ]; then
	echo "Usage: show_result reference_frames_dir [frames_dir1, ...]"
	exit 1
fi

activate_env
for var in "$@"; do
	if [ $var = $1 ]; then
		continue
	fi

	print_result $1 $var
done
