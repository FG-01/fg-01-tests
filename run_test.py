import sys

gait_dir = "gait_recognition"

sys.path.append(gait_dir)

from gait_recognition.human_pose_nn import HumanPoseIRNetwork
from gait_recognition.gait_nn import GaitNetwork
import json
import time
import os
import numpy
import tensorflow as tf
from parse_frames import parse_folder_numpy

if len(sys.argv) < 2:
    print("Usage: run_test.py video_folder")
    sys.exit(1)

net_pose = HumanPoseIRNetwork()
net_gait = GaitNetwork(recurrent_unit = 'GRU', rnn_layers = 2)

print("-----------> Parsing video frames...")
video_frames = parse_folder_numpy(sys.argv[1], (299, 299))
print("-----------> Finished parsing frames!")

#print(video_frames)

net_pose.restore(os.path.join(gait_dir, 'Human3.6m.ckpt'))
net_gait.restore(os.path.join(gait_dir, 'models/H3.6m-GRU-1.ckpt'))

spatial_features = net_pose.feed_forward_features(video_frames)

identification_vector = net_gait.feed_forward(spatial_features)

with open(os.path.join(sys.argv[1], 'features.npy'), 'wb') as f:
    numpy.save(f, spatial_features)

with open(os.path.join(sys.argv[1], 'vector.npy'), 'wb') as f:
    numpy.save(f, identification_vector)

print("Identification vector has been written to", os.path.join(sys.argv[1], 'vector.npy'))

#print(identification_vector)

#with open("id_vector_" + time.time() + ".json", 'w') as f:
#    json.dump(identification_vector, f)
